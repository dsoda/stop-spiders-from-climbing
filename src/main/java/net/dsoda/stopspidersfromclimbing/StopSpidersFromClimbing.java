package net.dsoda.stopspidersfromclimbing;

import net.fabricmc.api.ModInitializer;
import net.minecraft.block.Block;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.util.Identifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StopSpidersFromClimbing implements ModInitializer {

	public static final String MOD_ID = "stopspidersfromclimbing";
    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);
	public static final TagKey<Block> SPIDER_NON_CLIMBABLE =
		TagKey.of(RegistryKeys.BLOCK, Identifier.of(MOD_ID, "spider_non_climbable"));

	@Override
	public void onInitialize() {
		LOGGER.info("Initializing \"Stop Spiders From Climbing\"");
	}
}