package net.dsoda.stopspidersfromclimbing.mixin;

import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import net.dsoda.stopspidersfromclimbing.StopSpidersFromClimbing;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.entity.mob.SpiderEntity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.At;

@Mixin(SpiderEntity.class)
public abstract class SpiderEntityMixin extends HostileEntity {

	public SpiderEntityMixin(EntityType<? extends SpiderEntity> entityType, World world) {
		super(entityType, world);
	}

	@Unique
	private byte stopspidersfromclimbing$touchingNonClimbable() {
		return (byte)(this.getWorld().getBlockState(this.getBlockPos().offset(this.getHorizontalFacing()))
			.isIn(StopSpidersFromClimbing.SPIDER_NON_CLIMBABLE)?1:0);
	}

	@WrapOperation(
		method = "setClimbingWall",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/entity/data/DataTracker;set(Lnet/minecraft/entity/data/TrackedData;Ljava/lang/Object;)V"
		)
	)
	private <T> void setSpiderFlags(DataTracker instance, TrackedData<Byte> key, T value, Operation<Void> original) {
		instance.set(key, (byte)((byte)value & ~this.stopspidersfromclimbing$touchingNonClimbable()));
	}
}