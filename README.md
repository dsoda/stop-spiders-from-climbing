# Stop Spiders From Climbing!

A simple mod that adds a tag "spider_non_climbable" which allows defining blocks which spiders are unable to climb. By default, spiders are unable to climb ice, packed ice, blue ice, and every color of glazed terracotta. This list can be modified using data packs.
